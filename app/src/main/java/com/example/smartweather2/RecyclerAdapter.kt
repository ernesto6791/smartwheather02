package com.example.smartweather2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter(private val days: List<Day>) :  RecyclerView.Adapter<RecyclerAdapter.ViewHolder>(){

    private val title = "Chapter One"
    private val detail = "Item one details"
    private val image = R.drawable.wheather_image_1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val day = days[position]
        viewHolder.itemTitle.text = day.message
        viewHolder.itemDetail.text = detail
        viewHolder.itemImage.setImageResource(image)
    }

    override fun getItemCount() = days.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemImage: ImageView
        var itemTitle: TextView
        var itemDetail: TextView
        init {
            itemImage = itemView.findViewById(R.id.item_image)
            itemTitle = itemView.findViewById(R.id.item_title)
            itemDetail = itemView.findViewById(R.id.item_detail)
        }
    }

}