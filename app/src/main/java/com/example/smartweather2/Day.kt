package com.example.smartweather2

data class Day (
    val cod: String,
    val message: String
)