package com.example.smartweather2

import retrofit2.Call
import retrofit2.http.GET


interface ApiService {
    @GET("/climate")
    fun fetchAllDays(): Call<List<Day>>
}